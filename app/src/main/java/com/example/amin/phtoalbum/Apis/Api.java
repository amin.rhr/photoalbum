package com.example.amin.phtoalbum.Apis;

import com.example.amin.phtoalbum.Model.Album;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    @GET("albums")
    Call<List<Album>> getAlbums();
}
