package com.example.amin.phtoalbum.Apis;

import com.example.amin.phtoalbum.Model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PhotoApi {
    @GET("photos")
    Call<List<Photo>> getPhotos();
}
