package com.example.amin.phtoalbum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.example.amin.phtoalbum.Adapter.AlbumAdapter;
import com.example.amin.phtoalbum.Adapter.PhotoAdapter;
import com.example.amin.phtoalbum.Apis.PhotoApi;
import com.example.amin.phtoalbum.Model.Album;
import com.example.amin.phtoalbum.Model.Photo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhotoActivity extends AppCompatActivity implements PhotoAdapter.OnItemClickListener {

    private RecyclerView recyclerViewPhotoList;
    private List<Photo> photos;
    private PhotoAdapter photoAdapter;
    private List<Photo> photoTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        recyclerViewPhotoList = findViewById(R.id.recycler_view_photos);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PhotoApi photoApi = retrofit.create(PhotoApi.class);
        Call<List<Photo>> call = photoApi.getPhotos();

        Intent mIntent = getIntent();
        String albumTitle = mIntent.getStringExtra("title");
        setTitle(albumTitle);
        final int albumId = mIntent.getIntExtra("albumId ", 0);
        Log.d("id", "onCreate: " + albumId);

        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                photos = response.body();
                List<Photo> photoTemp = new ArrayList<>();
                /*for (Photo photo : photos) {
                    if (photo.getAlbumId() == albumId) {
                        photoTemp.add(photo);
                    }
                }*/
                photoAdapter = new PhotoAdapter(photos, PhotoActivity.this);
                //photoAdapter = new PhotoAdapter(photoTemp, PhotoActivity.this);
                recyclerViewPhotoList.setLayoutManager(new GridLayoutManager(PhotoActivity.this, 2));
                recyclerViewPhotoList.setAdapter(photoAdapter);
                photoAdapter.setOnItemClickListener(PhotoActivity.this);
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        Intent photoIntent = new Intent(this , Maximize.class);
        Photo clickedPhoto = photos.get(position);

        photoIntent.putExtra("url" ,clickedPhoto.getUrl());
        photoIntent.putExtra("PhotoTitle" ,clickedPhoto.getTitle());
        startActivity(photoIntent);
    }
}
