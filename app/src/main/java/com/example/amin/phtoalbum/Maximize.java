package com.example.amin.phtoalbum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Maximize extends AppCompatActivity {
    ImageView imgMaximizedPhoto;
    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maximize);

        imgMaximizedPhoto = findViewById(R.id.imgMaximizedPhoto);
        txtTitle = findViewById(R.id.txtTitle);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        String title = intent.getStringExtra("PhotoTitle");

        Picasso.with(this)
                .load(url)
                .into(imgMaximizedPhoto);
        txtTitle.setText(title);
    }
}
