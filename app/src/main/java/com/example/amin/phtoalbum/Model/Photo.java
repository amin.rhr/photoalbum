package com.example.amin.phtoalbum.Model;

public class Photo {
    /*"albumId": 1,
            "id": 1,
            "title": "accusamus beatae ad facilis cum similique qui sunt",
            "url": "https://via.placeholder.com/600/92c952",
            "thumbnailUrl": "https://via.placeholder.com/150/92c952"*/
    private int albumId;
    private int id;
    private String title;
    private String url;
    private String thumnailUrl;

    public int getAlbumId() {
        return albumId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getThumnailUrl() {
        return thumnailUrl;
    }
}
